package in.questvarsity.api.dao;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class QuestionDao {

	private String questionCode;
	private String questionType;
	private String questionData;
	private String questionAdditionalDetails;
	private int questionWeightage;
	private double questionRating;
	private long questionRatingCount;
	private String courseCode;
	private String universityCode;
	private boolean isVerified;
	private boolean isEnabled;

	public QuestionDao() {
	}

	public String getQuestionAdditionalDetails() {
		return questionAdditionalDetails;
	}

	public void setQuestionAdditionalDetails(String questionAdditionalDetails) {
		this.questionAdditionalDetails = questionAdditionalDetails;
	}

	public boolean isVerified() {
		return isVerified;
	}

	public void setVerified(boolean isVerified) {
		this.isVerified = isVerified;
	}

	public boolean isEnabled() {
		return isEnabled;
	}

	public void setEnabled(boolean isEnabled) {
		this.isEnabled = isEnabled;
	}

	public String getQuestionCode() {
		return questionCode;
	}

	public void setQuestionCode(String questionCode) {
		this.questionCode = questionCode;
	}

	public String getQuestionType() {
		return questionType;
	}

	public void setQuestionType(String questionType) {
		this.questionType = questionType;
	}

	public String getQuestionData() {
		return questionData;
	}

	public void setQuestionData(String questionData) {
		this.questionData = questionData;
	}

	public int getQuestionWeightage() {
		return questionWeightage;
	}

	public void setQuestionWeightage(int questionWeightage) {
		this.questionWeightage = questionWeightage;
	}

	public double getQuestionRating() {
		return questionRating;
	}

	public void setQuestionRating(double rating) {
		this.questionRating = rating;
	}

	public String getCourseCode() {
		return courseCode;
	}

	public void setCourseCode(String courseCode) {
		this.courseCode = courseCode;
	}

	public String getUniversityCode() {
		return universityCode;
	}

	public void setUniversityCode(String universityCode) {
		this.universityCode = universityCode;
	}

	public long getQuestionRatingCount() {
		return questionRatingCount;
	}

	public void setQuestionRatingCount(long questionRatingCount) {
		this.questionRatingCount = questionRatingCount;
	}

}
