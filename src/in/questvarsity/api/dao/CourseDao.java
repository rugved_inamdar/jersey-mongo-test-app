package in.questvarsity.api.dao;

public class CourseDao {

	private String courseCode;
	private String courseName;
	private String courseDescription;
	private String courseImageRating;
	private double courseRating;
	private String universityCode;
	private boolean isVerified;
	private boolean isEnabled;
	private long courseRatingCount;

	public CourseDao() {
	}

	public String getCourseCode() {
		return courseCode;
	}

	public void setCourseCode(String courseCode) {
		this.courseCode = courseCode;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getCourseDescription() {
		return courseDescription;
	}

	public void setCourseDescription(String courseDescription) {
		this.courseDescription = courseDescription;
	}

	public String getCourseImageRating() {
		return courseImageRating;
	}

	public void setCourseImageRating(String courseImageRating) {
		this.courseImageRating = courseImageRating;
	}

	public double getCourseRating() {
		return courseRating;
	}

	public void setCourseRating(double courseRating) {
		this.courseRating = courseRating;
	}

	public String getUniversityCode() {
		return universityCode;
	}

	public void setUniversityCode(String universityCode) {
		this.universityCode = universityCode;
	}

	public boolean isVerified() {
		return isVerified;
	}

	public void setVerified(boolean isVerified) {
		this.isVerified = isVerified;
	}

	public boolean isEnabled() {
		return isEnabled;
	}

	public void setEnabled(boolean isEnabled) {
		this.isEnabled = isEnabled;
	}

	public long getCourseRatingCount() {
		return courseRatingCount;
	}

	public void setCourseRatingCount(long courseRatingCount) {
		this.courseRatingCount = courseRatingCount;
	}

}
