package in.questvarsity.api.dao;

public class UniversityDao {

	private String universityCode;
	private String universityName;
	private String universityDescription;
	private String universityImageUrl;
	private String universityAddress;
	private boolean isVerified;
	private boolean isEnabled;
	private double universityRating;
	private long universityRatingCount;
	
	public UniversityDao() {
	}
	
	public String getUniversityCode() {
		return universityCode;
	}
	public void setUniversityCode(String universityCode) {
		this.universityCode = universityCode;
	}
	public String getUniversityName() {
		return universityName;
	}
	public void setUniversityName(String universityName) {
		this.universityName = universityName;
	}
	public String getUniversityDescription() {
		return universityDescription;
	}
	public void setUniversityDescription(String universityDescription) {
		this.universityDescription = universityDescription;
	}
	public String getUniversityImageUrl() {
		return universityImageUrl;
	}
	public void setUniversityImageUrl(String universityImageUrl) {
		this.universityImageUrl = universityImageUrl;
	}
	public String getUniversityAddress() {
		return universityAddress;
	}
	public void setUniversityAddress(String universityAddress) {
		this.universityAddress = universityAddress;
	}
	public boolean isVerified() {
		return isVerified;
	}
	public void setVerified(boolean isVerified) {
		this.isVerified = isVerified;
	}
	public boolean isEnabled() {
		return isEnabled;
	}
	public void setEnabled(boolean isEnabled) {
		this.isEnabled = isEnabled;
	}
	public double getUniversityRating() {
		return universityRating;
	}
	public void setUniversityRating(double universityRating) {
		this.universityRating = universityRating;
	}
	public long getUniversityRatingCount() {
		return universityRatingCount;
	}
	public void setUniversityRatingCount(long universityRatingCount) {
		this.universityRatingCount = universityRatingCount;
	}
	
	
}
