package in.questvarsity.api;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.servlet.GuiceServletContextListener;
import com.google.inject.servlet.ServletModule;

public class Main extends GuiceServletContextListener {
	public static Injector injector;

	@Override
	protected Injector getInjector() {
		injector = Guice.createInjector(new ServletModule() {
			// Configure your IOC
			@Override
			protected void configureServlets() {
				// bind(Service.class);
			}
		});

		return injector;
	}

}
