package in.questvarsity.api.service;

import java.util.List;

import in.questvarsity.api.dao.QuestionDao;

public interface QuestionService {

	public QuestionDao getQuestion(String universityCode, String courseCode, String questionCode);

	public List<QuestionDao> getAllQuestionForUniversity(String universityCode);

	public List<QuestionDao> getAllQuestionForCourse(String universityCode, String courseCode);
	
	public boolean setQuestion(QuestionDao question);

}
