package in.questvarsity.api.service.impl;

import in.questvarsity.api.dao.QuestionDao;
import in.questvarsity.api.service.QuestionService;
import in.questvarsity.api.utils.ApplicationConstants;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.WriteConcern;
import com.mongodb.WriteResult;

public class QuestionServiceImpl implements QuestionService {
	MongoClient mongoClient;
	DB questVarsityDb;
	DBCollection questionCollection;

	public QuestionServiceImpl() {
		try {
			mongoClient = new MongoClient(ApplicationConstants.HOST, ApplicationConstants.PORT);
			mongoClient.setWriteConcern(WriteConcern.JOURNALED);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		questVarsityDb = mongoClient.getDB(ApplicationConstants.DB_NAME);
		boolean collectionExists = questVarsityDb.collectionExists(ApplicationConstants.COLLECTION_QUESTION);
		if (collectionExists == false) {
			questionCollection = questVarsityDb.createCollection(ApplicationConstants.COLLECTION_QUESTION, null);
		} else {
			questionCollection = questVarsityDb.getCollection(ApplicationConstants.COLLECTION_QUESTION);
		}
	}

	@Override
	public QuestionDao getQuestion(String universityCode, String courseCode, String questionCode) {
		BasicDBObject queryObject = new BasicDBObject();
		queryObject.append(ApplicationConstants.KEY_UNIVERSITY_CODE, universityCode);
		queryObject.append(ApplicationConstants.KEY_COURSE_CODE, courseCode);
		queryObject.append(ApplicationConstants.KEY_QUESTION_CODE, questionCode);
		DBCursor cursor = questionCollection.find(queryObject);
		try {
			if (cursor.count() == 0) {
				System.out.println(cursor.one());
				BasicDBObject question = (BasicDBObject) cursor.one();
				QuestionDao questionObject = new QuestionDao();
				questionObject.setQuestionCode(question.getString(ApplicationConstants.KEY_QUESTION_CODE));
				questionObject.setQuestionType(question.getString(ApplicationConstants.KEY_QUESTION_TYPE));
				questionObject.setQuestionData(question.getString(ApplicationConstants.KEY_QUESTION_DATA));
				questionObject.setUniversityCode(question.getString(ApplicationConstants.KEY_UNIVERSITY_CODE));
				questionObject.setCourseCode(question.getString(ApplicationConstants.KEY_COURSE_CODE));
				questionObject.setQuestionWeightage(question.getInt(ApplicationConstants.KEY_QUESTION_WEIGHTAGE, 0));
				questionObject.setQuestionRating(question.getDouble(ApplicationConstants.KEY_QUESTION_RATING));
				// questionObject.setAskedYear((List<String>)
				// question.get(ApplicationConstants.KEY_QUESTION_ASKED_YEAR));
				return questionObject;
			}
		} finally {
			cursor.close();
		}
		return null;
	}

	@Override
	public List<QuestionDao> getAllQuestionForUniversity(String universityCode) {
		BasicDBObject queryObject = new BasicDBObject();
		queryObject.append(ApplicationConstants.KEY_UNIVERSITY_CODE, universityCode);
		DBCursor cursor = questionCollection.find(queryObject);
		List<QuestionDao> questionList = new ArrayList<QuestionDao>();
		try {
			while (cursor.hasNext()) {
				System.out.println(cursor.one());
				BasicDBObject question = (BasicDBObject) cursor.next();
				QuestionDao questionObject = new QuestionDao();
				questionObject.setQuestionCode(question.getString(ApplicationConstants.KEY_QUESTION_CODE));
				questionObject.setQuestionType(question.getString(ApplicationConstants.KEY_QUESTION_TYPE));
				questionObject.setQuestionData(question.getString(ApplicationConstants.KEY_QUESTION_DATA));
				questionObject.setUniversityCode(question.getString(ApplicationConstants.KEY_UNIVERSITY_CODE));
				questionObject.setCourseCode(question.getString(ApplicationConstants.KEY_COURSE_CODE));
				questionObject.setQuestionWeightage(question.getInt(ApplicationConstants.KEY_QUESTION_WEIGHTAGE, 0));
				questionObject.setQuestionRating(question.getDouble(ApplicationConstants.KEY_QUESTION_RATING));
				questionList.add(questionObject);
			}
		} finally {
			cursor.close();
		}
		return questionList;
	}

	@Override
	public List<QuestionDao> getAllQuestionForCourse(String universityCode, String courseCode) {
		BasicDBObject queryObject = new BasicDBObject();
		queryObject.append(ApplicationConstants.KEY_UNIVERSITY_CODE, universityCode);
		queryObject.append(ApplicationConstants.KEY_COURSE_CODE, courseCode);
		DBCursor cursor = questionCollection.find(queryObject);
		List<QuestionDao> questionList = new ArrayList<QuestionDao>();
		try {
			while (cursor.hasNext()) {
				System.out.println(cursor.one());
				BasicDBObject question = (BasicDBObject) cursor.next();
				QuestionDao questionObject = new QuestionDao();
				questionObject.setQuestionCode(question.getString(ApplicationConstants.KEY_QUESTION_CODE));
				questionObject.setQuestionType(question.getString(ApplicationConstants.KEY_QUESTION_TYPE));
				questionObject.setQuestionData(question.getString(ApplicationConstants.KEY_QUESTION_DATA));
				questionObject.setUniversityCode(question.getString(ApplicationConstants.KEY_UNIVERSITY_CODE));
				questionObject.setCourseCode(question.getString(ApplicationConstants.KEY_COURSE_CODE));
				questionObject.setQuestionWeightage(question.getInt(ApplicationConstants.KEY_QUESTION_WEIGHTAGE, 0));
				questionObject.setQuestionRating(question.getDouble(ApplicationConstants.KEY_QUESTION_RATING));
				questionList.add(questionObject);
			}
		} finally {
			cursor.close();
		}
		return questionList;
	}

	@Override
	public boolean setQuestion(QuestionDao question) {
		BasicDBObject dbObject = new BasicDBObject();
		dbObject.append(ApplicationConstants.KEY_QUESTION_CODE, question.getQuestionCode());
		dbObject.append(ApplicationConstants.KEY_QUESTION_TYPE, question.getQuestionType());
		dbObject.append(ApplicationConstants.KEY_QUESTION_DATA, question.getQuestionData());
		dbObject.append(ApplicationConstants.KEY_QUESTION_WEIGHTAGE, question.getQuestionWeightage());
		dbObject.append(ApplicationConstants.KEY_QUESTION_RATING, question.getQuestionRating());
		dbObject.append(ApplicationConstants.KEY_COURSE_CODE, question.getCourseCode());
		dbObject.append(ApplicationConstants.KEY_UNIVERSITY_CODE, question.getUniversityCode());
		WriteResult result = questionCollection.insert(dbObject);
		long success = questionCollection.count(new BasicDBObject("_id", dbObject.get("_id")));
		if (success == 1) {
			return true;
		} else
			return false;
	}

}
