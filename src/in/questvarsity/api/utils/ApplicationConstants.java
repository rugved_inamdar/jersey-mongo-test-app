package in.questvarsity.api.utils;

public class ApplicationConstants {

	public static final String HOST = "localhost";
	public static final int PORT = 27017;
	public static final String DB_NAME = "questVarsity";

	public static final String COLLECTION_QUESTION = "Question";
	public static final String KEY_QUESTION_CODE = "questionCode";
	public static final String KEY_QUESTION_TYPE = "questionType";
	public static final String KEY_QUESTION_DATA = "questionData";
	public static final String KEY_QUESTION_WEIGHTAGE = "questionWeightage";
	public static final String KEY_QUESTION_ASKED_YEAR = "questionAskedYear";
	public static final String KEY_QUESTION_RATING = "questionRating";
	
	public static final String COLLECTION_COURSE = "Course";
	public static final String KEY_COURSE_CODE = "courseCode";
	
	
	
	public static final String COLLECTION_UNIVERSITY = "University";
	public static final String KEY_UNIVERSITY_CODE = "universityCode";

}
