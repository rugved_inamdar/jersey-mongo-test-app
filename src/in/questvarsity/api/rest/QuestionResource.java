package in.questvarsity.api.rest;

import java.util.List;

import in.questvarsity.api.dao.QuestionDao;
import in.questvarsity.api.service.impl.QuestionServiceImpl;
import in.questvarsity.api.utils.ApplicationConstants;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBElement;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

@Path("/question")
public class QuestionResource {
	QuestionServiceImpl questionService;

	public QuestionResource() {
		questionService = new QuestionServiceImpl();
	}

	// This method is called if TEXT_PLAIN is request
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String getVersion() {
		return "1.0";
	}

	// This method is called if XML is request
	@GET
	@Produces(MediaType.TEXT_XML)
	public String getVersionXML() {
		return "<?xml version=\"1.0\"?>" + "<version>1.0" + "</version>";
	}

	// This method is called if HTML is request
	@Path("/version")
	@GET
	@Produces(MediaType.TEXT_HTML)
	public String getVersionHtml() {
		return "<html> " + "<title>" + "1.0" + "</title>" + "<body><h1>" + "Questvarsity Question API version 1.0" + "</body></h1>" + "</html> ";
	}

	@POST
	@Path("/getQuestion")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public String getQuestion(@FormParam("questionCode") String questionCode, @FormParam("courseCode") String courseCode, @FormParam("universityCode") String universityCode) {

		QuestionDao question = questionService.getQuestion(universityCode, courseCode, questionCode);
		JSONObject response = new JSONObject();
		response.put(ApplicationConstants.KEY_COURSE_CODE, question.getCourseCode());
		response.put(ApplicationConstants.KEY_UNIVERSITY_CODE, question.getUniversityCode());
		response.put(ApplicationConstants.KEY_QUESTION_CODE, question.getQuestionCode());
		response.put(ApplicationConstants.KEY_QUESTION_DATA, question.getQuestionData());
		response.put(ApplicationConstants.KEY_QUESTION_TYPE, question.getQuestionType());
		response.put(ApplicationConstants.KEY_QUESTION_WEIGHTAGE, question.getQuestionWeightage());
		response.put(ApplicationConstants.KEY_QUESTION_RATING, question.getQuestionRating());
		return response.toJSONString();
	}

	@POST
	@Path("/{universityCode}/{courseCode}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public String getAllQuestionForCourse(@PathParam("courseCode") String courseCode, @PathParam("universityCode") String universityCode) {

		List<QuestionDao> questionList = questionService.getAllQuestionForCourse(universityCode, courseCode);
		JSONArray responseArray = new JSONArray();
		for (QuestionDao question : questionList) {
			JSONObject response = new JSONObject();
			response.put(ApplicationConstants.KEY_COURSE_CODE, question.getCourseCode());
			response.put(ApplicationConstants.KEY_UNIVERSITY_CODE, question.getUniversityCode());
			response.put(ApplicationConstants.KEY_QUESTION_CODE, question.getQuestionCode());
			response.put(ApplicationConstants.KEY_QUESTION_DATA, question.getQuestionData());
			response.put(ApplicationConstants.KEY_QUESTION_TYPE, question.getQuestionType());
			response.put(ApplicationConstants.KEY_QUESTION_WEIGHTAGE, question.getQuestionWeightage());
			response.put(ApplicationConstants.KEY_QUESTION_RATING, question.getQuestionRating());

			responseArray.add(response);
		}
		return responseArray.toJSONString();
	}

	@POST
	@Path("/{universityCode}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public String getAllQuestionForUniversity(@PathParam("universityCode") String universityCode) {

		List<QuestionDao> questionList = questionService.getAllQuestionForUniversity(universityCode);
		JSONArray responseArray = new JSONArray();
		for (QuestionDao question : questionList) {
			JSONObject response = new JSONObject();
			response.put(ApplicationConstants.KEY_COURSE_CODE, question.getCourseCode());
			response.put(ApplicationConstants.KEY_UNIVERSITY_CODE, question.getUniversityCode());
			response.put(ApplicationConstants.KEY_QUESTION_CODE, question.getQuestionCode());
			response.put(ApplicationConstants.KEY_QUESTION_DATA, question.getQuestionData());
			response.put(ApplicationConstants.KEY_QUESTION_TYPE, question.getQuestionType());
			response.put(ApplicationConstants.KEY_QUESTION_WEIGHTAGE, question.getQuestionWeightage());
			response.put(ApplicationConstants.KEY_QUESTION_RATING, question.getQuestionRating());

			responseArray.add(response);
		}
		return responseArray.toJSONString();
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public String setQuestion(@FormParam("questionCode") String questionCode, @FormParam("questionData") String questionData, @FormParam("questionType") String questionType,
			@FormParam("askedYear") List<String> askedYear, @FormParam("courseCode") String courseCode, @FormParam("universityCode") String universityCode,
			@FormParam("questionWeightage") int questionWeightage, @FormParam("questionRating") double questionRating) {
		QuestionDao question = new QuestionDao();
		if (questionCode != null) {
			question.setQuestionCode(questionCode);
		}
		if (questionType != null) {
			question.setQuestionType(questionType);
			;
		}
		if (questionData != null) {
			question.setQuestionData(questionData);
			;
		}
		if (courseCode != null) {
			question.setCourseCode(courseCode);
		}
		if (universityCode != null) {
			question.setUniversityCode(universityCode);
		}
		question.setQuestionWeightage(questionWeightage);
		question.setQuestionRating(questionRating);

		boolean isSuccess = questionService.setQuestion(question);
		if (isSuccess) {
			return "{\"data\":\"success\" }";
		} else
			return "{\"data\":\"fail\" }";
	}

}
